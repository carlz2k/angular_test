'use strict';

var app = require('../app');
var Promise = require('bluebird');

app.service('FileReader', function () {
    return {
        CONTENT_TYPE: {
            IMAGE : /image.*/
        },

        read: function (fileElement, contentType, success, fail) {
            if (window.File && window.FileReader && window.FileList && window.Blob) {
                fileElement.addEventListener('change', function () {
                    //Set the extension for the file
                    //var fileExtension = /image.*/;
                    //Get the file object
                    var fileTobeRead = fileElement.files[0];
                    //Check of the extension match
                    if (fileTobeRead.type.match(contentType)) {
                        //Initialize the FileReader object to read the file
                        var fileReader = new window.FileReader();
                        fileReader.onload = function () {
                            var dataUrl = fileReader.result;
                            if (dataUrl) {
                                success(dataUrl);
                            } else {
                                fail('cannot read the image properly');
                            }
                        };
                        fileReader.readAsDataURL(fileTobeRead);
                    } else {
                        fail('not an image file');
                    }

                }, false);
            } else {
                fail('your browser does not support file uploading');
            }
        }
    };
});
