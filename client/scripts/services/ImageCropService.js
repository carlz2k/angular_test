'use strict';

var app = require('../app');

app.service('ImageCropService', function () {
    var darkRoomJs;
    //https://github.com/ebidel/filer.js/blob/master/src/filer.js#L137
    function toBlob(dataURL) {
        var BASE64_MARKER = ';base64,';
        var parts, contentType, raw;

        if (dataURL.indexOf(BASE64_MARKER) === -1) {
            parts = dataURL.split(',');
            contentType = parts[0].split(':')[1];
            raw = decodeURIComponent(parts[1]);

            return new window.Blob([raw], {
                type: contentType
            });
        } else {
            parts = dataURL.split(BASE64_MARKER);
            contentType = parts[0].split(':')[1];
            raw = window.atob(parts[1]);
            var rawLength = raw.length;

            var uInt8Array = new Uint8Array(rawLength);

            for (var i = 0; i < rawLength; ++i) {
                uInt8Array[i] = raw.charCodeAt(i);
            }


            return new window.Blob([uInt8Array], {
                type: contentType
            });
        }


    }

    //@author: carl
    //there is a bug with darkroomjs, every new instance of darkroomjs automatically
    //creates a new canvas container and the existing ones are not deleted, which
    //does not work for us as we need the new image source to replace the existing
    //image source.
    //thus I have to delete the existing canvas container manually before creating
    //a new darkroomjs object, hope it does not cause any side-effects
    function cleanUpExistingCotainers() {
        if (darkRoomJs) {
            var existingContainerElement = darkRoomJs.containerElement,
                originalImageElement = darkRoomJs.originalImageElement;

            existingContainerElement.parentElement
                .replaceChild(originalImageElement, existingContainerElement);
        }
    }

    //the function is created to bypass the lint rule
    //that prevents me from using new as a side-effect
    //which is actually a valid concern
    function createDarkRoomObject(element, saveCallback) {

        cleanUpExistingCotainers();
        darkRoomJs = new window.Darkroom('#' + element.id, {

            plugins: {
                crop: {
                    minHeight: 50,
                    minWidth: 50
                },
                save: {
                    callback: function () {
                        saveCallback(toBlob(this.darkroom.canvas.toDataURL()));
                    }
                }
            },

            // Post initialization method
            initialize: function () {
                // Active crop selection
                this.plugins.crop.requireFocus();
            }
        });
    }

    return {
        attachToElement: function (element, saveCallback) {
            createDarkRoomObject(element, saveCallback);
        }
    };
});
