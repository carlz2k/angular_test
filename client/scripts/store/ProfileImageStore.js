'use strict';

var app = require('../app');

var ON_PROFILE_IMAGE_CROPPING_UPDATED = 'ON_PROFILE_IMAGE_CROPPING_UPDATED';

app.service('ProfileImageStore', function ($rootScope) {

    var image;
    var imageForCropping;
    return {
        onProfileImageCroppingUpdated: function(scope, callback){
            scope.$on(ON_PROFILE_IMAGE_CROPPING_UPDATED, function(){
                callback(imageForCropping);
            });
        },
        loadImage: function (p) {
            image = p;
        },
        getImageForCopping: function () {
            if(imageForCropping){
                $rootScope.$broadcast(ON_PROFILE_IMAGE_CROPPING_UPDATED);
            }
        },
        updateImageForCropping: function (p) {
            imageForCropping = p;
            $rootScope.$broadcast(ON_PROFILE_IMAGE_CROPPING_UPDATED);
        }
    };
});
