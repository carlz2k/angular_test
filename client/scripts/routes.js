'use strict';

require('./controllers/MainController');
require('./controllers/ImageEditingController');

var routes = [
  {
    stateName: 'image',
    url:'/image',
    templateUrl:'../templates/image_editing.html',
      controller: 'ImageEditingController'
  }
];

module.exports = routes;
