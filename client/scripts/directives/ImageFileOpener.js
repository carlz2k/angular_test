'use strict';

var app = require('../app');
var angular = require('angular');
require('../services/FileReader');
require('../services/ImageCropService');
require('../store/ProfileImageStore');

app.directive('imagefileopener', function ($http, FileReader, ProfileImageStore) {

    return {
        link: function (scope, elem) {
            FileReader.read(elem[0], FileReader.CONTENT_TYPE.IMAGE,
                function(imageDataUrl){
                    ProfileImageStore.updateImageForCropping(imageDataUrl);
                },
                function(errorMessage){
                    console.log(errorMessage);
                }
            );
        }
    };
});
