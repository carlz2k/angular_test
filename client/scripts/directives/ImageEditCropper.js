'use strict';

var app = require('../app');
var $ = require('jquery');
require('cropper');

app.directive('imageeditcropper', function () {
    return {
        replace: true,
        scope: true,
        link: function (scope, elem, attrs) {
            $('#'+attrs.id).cropper({
                crop: function() {
                    var canvas = $('#'+attrs.id).cropper('getCroppedCanvas');
                    console.log(canvas.toDataURL());
                }
            });
        }
    };
});
