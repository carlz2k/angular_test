'use strict';

var app = require('../app');
require('../store/ProfileImageStore');

app.directive('imageedit', function ($http, ImageCropService, ProfileImageStore) {
    function handleSaveImage(blobData) {
        var fd = new window.FormData();
        fd.append('file', blobData);
        $http.post('http://localhost:8080/receipt_images/53', fd, {
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (res) {
            console.log(res);
        }).error(function (err) {
            console.log(err);
        });
    }

    function crop(elem, image){
        elem.src = image;
        ImageCropService.attachToElement(elem, handleSaveImage);
    }

    return {

        link: function (scope, elem) {
            ProfileImageStore.onProfileImageCroppingUpdated(scope, function(image){
                var imageElement = elem[0];
                if(imageElement){
                    crop(imageElement, image);
                }
            });
            ProfileImageStore.getImageForCopping();
        }
    };
});
