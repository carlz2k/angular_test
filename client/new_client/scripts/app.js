"use srict";
var angular = require('angular'),
  log = require('loglevel');
require('angular-ui-router');
require('angular-ui-bootstrap');
var _ = require('lodash');
var app = angular.module('AngularTest', [
  'ui.router',
  'ui.bootstrap'
]);

log.setDefaultLevel('debug');
module.exports = app;


app.config(
  function ($httpProvider, $stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/image');
  });

app.run(function ($rootScope, $state) {
  log.debug("start app 2");
  $rootScope.$state = $state;
  log.debug("start app 2");
});

