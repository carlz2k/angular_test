'use strict';

var gulp = require('gulp');

require('./gulp/gulp-build-client');
require('./gulp/gulp-build-server');
require('./gulp/gulp-test');

gulp.task('develop', ['server-run', 'client-changes-watch']);

gulp.task('develop-client', ['client-changes-watch']);



