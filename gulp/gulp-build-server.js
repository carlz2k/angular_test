'use strict';

var gulp = require('gulp');
var nodemon = require('gulp-nodemon');

gulp.task('server-run', function () {
  nodemon({
    script: 'server/app.js'
    , watch: ['server/lib','server']
  })
});
