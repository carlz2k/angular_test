'use strict';

var gulp = require('gulp');
var mocha = require('gulp-mocha');

gulp.task('server-test', function () {
  return gulp.src(['test/**/*.js'], { read: false })
    .pipe(mocha())
    .on('error', function() {
      exit(1);
    });
});

